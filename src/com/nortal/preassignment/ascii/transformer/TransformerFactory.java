package com.nortal.preassignment.ascii.transformer;

public final class TransformerFactory {

  private TransformerFactory() {
  }

  public static CharacterTransformer createTransformer() {
    return createSimplePositionBasedTransformer();
  }

  public static CharacterTransformer createSimplePositionBasedTransformer() {
    return new SimplePositionBasedTransformer();
  }
}
