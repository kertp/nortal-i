package com.nortal.preassignment.ascii.transformer;

/**
 * @author Priit Liivak
 * 
 */
public class SimplePositionBasedTransformer implements CharacterTransformer {


  @Override
  public char encode(char input, int lineIndx, int colIndx) {
    return (char) (input + colIndx - lineIndx);
  }

  @Override
  public char decode(char input, int lineIndx, int colIndx) {
    return (char) (input - colIndx + lineIndx);
  }

}
