package com.nortal.preassignment.ascii.transformer;

/**
 * Transformer that uses a two way function to either encode or decode a
 * character.
 * 
 * @author Priit Liivak
 */
public interface CharacterTransformer {

  /**
   * @param lineIndx
   *          line position of the character in matrix
   * @param colIndx
   *          column position of the character in matrix (line)
   */
  char encode(char input, int lineIndx, int colIndx);

  /**
   * @param lineIndx
   *          line position of the character in matrix
   * @param colIndx
   *          column position of the character in matrix (line)
   */
  char decode(char input, int lineIndx, int colIndx);

}
