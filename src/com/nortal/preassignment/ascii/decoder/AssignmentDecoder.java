package com.nortal.preassignment.ascii.decoder;

import java.io.IOException;

import com.nortal.preassignment.ascii.transformer.CharacterTransformer;
import com.nortal.preassignment.ascii.transformer.TransformerFactory;

/**
 * @author Priit Liivak
 * 
 */
public abstract class AssignmentDecoder {

  public static final String INPUT_FILE_NAME = "ascii-assignment-input.txt";

  /**
   * Read the file to character matrix. For example if file would contain:
   * 
   * <pre>
   * 123
   * 567
   * </pre>
   * 
   * Then result matrix would contain
   * 
   * <pre>
   * [ [1,2,3], [5,6,7] ]
   * </pre>
   * 
   * and result[0][1] would return second character from first row (2)
   * 
   * @param fileName the name of the file to be read.
   * 
   * @return matrix of input file characters
   */
  public abstract char[][] readFileToMatrix(String fileName) throws IOException;

  /**
   * Decode each character in input matrix to output matrix using given
   * transformer. For each item do
   * 
   * <pre>
   * char inputChar = input[i][j];
   * output[i][j] = trasformer.decode(inputChar, i, j);
   * </pre>
   * 
   * <b> NB! You also need to improve the transformer that is used </b> <br/>
   * 
   * @param input[][] 2d matrix to decode.
   * 
   * @param transformer transformer describing the chiper.
   * 
   * @return the decoded 2d matrix.
   */
  public abstract char[][] decodeMatrix(char[][] input, CharacterTransformer trasformer);

  /**
   * 
   * Actual decode method that is called.
   */
  public void decode() throws IOException {
    char[][] artMatrix = readFileToMatrix(INPUT_FILE_NAME);
    CharacterTransformer trasformer = TransformerFactory.createTransformer();
    char[][] decodedMatrix = decodeMatrix(artMatrix, trasformer);
    printMatrix(decodedMatrix);
  }
  
   /**
   * 
   * Method for printing out 2D matrix.
   * 
   * @param matrix[][] 2D matrix to be printed out.
   */
  private void printMatrix(char[][] matrix) {
    for (int lineIndx = 0; lineIndx < matrix.length; lineIndx++) {
      for (int colIndx = 0; colIndx < matrix[lineIndx].length; colIndx++){
        System.out.print(matrix[lineIndx][colIndx]);
      }
      System.out.println();
    }
  }
}
