/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nortal.preassignment.ascii.decoder;

import com.nortal.preassignment.ascii.transformer.CharacterTransformer;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
/**
 *
 * @author kert
 */
public class AssignmentDecoderSolution extends AssignmentDecoder{

    @Override
    public char[][] readFileToMatrix(String fileName) throws IOException {
        String line;
        char[][] textList;
        ArrayList<String> textArray = new ArrayList<>();
        try {
            BufferedReader br;
            br = new BufferedReader(new FileReader(fileName));
            while ((line = br.readLine()) != null) {
                textArray.add(line);
            }
        }
        catch (IOException e) {
            System.err.println("Error: " + e);
        }
        textList = new char[textArray.size()][];
        for(int i = 0; i < textArray.size() ; i++){
            textList[i] = textArray.get(i).toCharArray();
        }
        return textList;
    }

    @Override
    public char[][] decodeMatrix(char[][] input, CharacterTransformer transformer) {
      char[][] decoded = new char[input.length][];
      for (int rowIndx = 0; rowIndx < input.length; rowIndx++) {
        // Array that holds the decoded chars of the current row.
        char[] decodedRow = new char[input[rowIndx].length];
        // Iterating through the chars of the current row.
        for (int colIndx = 0; colIndx < input[rowIndx].length; colIndx++) {
          // Decoding the char and adding it to decodedRow.
          decodedRow[colIndx] = (transformer.decode(input[rowIndx][colIndx], rowIndx, colIndx));
        }
        // Adding the decoded row to 2D matrix.
        decoded[rowIndx] = decodedRow;
      } 
      return decoded;
    }   
}