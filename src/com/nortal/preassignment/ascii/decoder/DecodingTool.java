package com.nortal.preassignment.ascii.decoder;


/**
 * @author Priit Liivak
 * 
 */
public class DecodingTool {

  /**
   * Take a look at AssignmentDecoder abstract class. Your assignment is to:
   * <ol>
   * <li>Unconmment the contents of main</li>
   * <li>Create a concrete implementation of AssignmentDecoder following the comments on AssignmentDecoder abstract methods</li>
   * <li>Run the program</li>
   * <li>Send the result to Age :)</li>
   * </ol>
   * 
   */
  public static void main(String[] args) throws Exception {
     AssignmentDecoder decoder = new AssignmentDecoderSolution();
      char[][] readFileToMatrix;
      readFileToMatrix = decoder.readFileToMatrix(AssignmentDecoder.INPUT_FILE_NAME);
      decoder.decode();      
  }
}
